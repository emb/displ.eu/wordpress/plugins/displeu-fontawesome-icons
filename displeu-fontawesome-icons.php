<?php
/**
 * Plugin Name: DisplEU FontAwesome Icons
 * Text Domain: displeu-fontawesome-icons
 * Plugin URI: https://gitlab.belowtoxic.cloud/wordpress/plugins/displeu-fontawesome-icons
 * Description: Integrate Fontawesome icons into your WordPress site.
 * Version: 1.0
 * Author: Oliver Maklott <oliver.maklott@fairkom.eu>
 * Author URI: https://git.fairkom.net/emb/displ.eu/wordpress/plugins/displeu-fontawesome-icons
 * License: GPL2
 */

// Prevent direct access to the file
defined('ABSPATH') or die('No script kiddies please!');

define('DPE__FONTAWESOME__LIBRARY_VERSION', '6.5.2');

// Define constant for plugin path
define('DPE__FONTAWESOME__PLUGIN_DIR', plugin_dir_path(__FILE__));
define('DPE__FONTAWESOME__PUBLIC_DIR', DPE__FONTAWESOME__PLUGIN_DIR . 'public/');
define('DPE__FONTAWESOME__ASSETS_DIR', DPE__FONTAWESOME__PUBLIC_DIR . 'assets/');

define('DPE__FONTAWESOME__URL_PATH_PREFIX', '/wp-content'.explode('wp-content', DPE__FONTAWESOME__PLUGIN_DIR)[1]);


/**
 * Load css and optional scripts
 *
 * @return void
 */
function dpe__fontawesome__styles_scripts__frontent() {
    // Enqueue styles
    wp_enqueue_style(
        'dpe-fontawesome-library-styles',
        DPE__FONTAWESOME__URL_PATH_PREFIX.'public/assets/fontawesome/'.DPE__FONTAWESOME__LIBRARY_VERSION.'/web/css/all.min.css',
        array(),
        filemtime(
            DPE__FONTAWESOME__PLUGIN_DIR.'public/assets/fontawesome/'.DPE__FONTAWESOME__LIBRARY_VERSION.'/web/css/all.min.css'
        ),
        'all'
    );
}
add_action( 'wp_enqueue_scripts', 'dpe__fontawesome__styles_scripts__frontent', 20 );
add_action( 'admin_enqueue_scripts', 'dpe__fontawesome__styles_scripts__frontent', 20 );
