# DisplEU Fontawesome Icons

Simple plugin to integrate fontawesome icons for developers in Wordpress.

## Update icons

Although the plugin comes with a version of fontawesome alredy, you might want to update them.

Simply add the version you prefer to `public/assets/fontawesome/` and change `DPE__FONTAWESOME__LIBRARY_VERSION` to the desired version.

